/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webjavafiles;

import DBFiles.Image;
import DBFiles.User;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Daniel
 */
@Path("login")
public class LoginResource {
    
    EntityManagerFactory emf;
    EntityManager em;
    ArrayList<Image> uuseriArray;
    List uuserList;
    String jee;
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of LoginResource
     */
    public LoginResource() {
    }

    /**
     * Retrieves representation of an instance of webjavafiles.LoginResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("logIn/{ooooo}/{password}")
    //@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public JsonObject logIn(@PathParam("ooooo") String uname,
                            @PathParam("password")String pword) {
        boolean jeeben = false;
        emf = Persistence.createEntityManagerFactory("ImageSharePU");
        em = emf.createEntityManager();        
        
        em.getTransaction().begin();
        JsonObjectBuilder jBuilder = Json.createObjectBuilder();
        JsonObject jo = jBuilder.build();
        
        try{
        User loggingUser = (User)em.createNamedQuery("User.findByUname").setParameter("uname", uname).getSingleResult();
        List allUsers = em.createNamedQuery("User.findAll").getResultList();
        ArrayList<User> allUsersArr = new ArrayList<>(allUsers);
        for(User u : allUsersArr){
            if(u.getUid() == loggingUser.getUid() && u.getPword().equals(loggingUser.getPword())){
                jeeben = true;
                jo =  jsonUserBuilder(loggingUser,jeeben);
                
            }
        }
        }
        catch(Exception e){
            return jo;
        }
        
        return jo;
    }
    
    @POST
    @Path("newUser")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void createUser(@FormParam("username") String uname, @FormParam("pword") String pword) {
        
        
        emf = Persistence.createEntityManagerFactory("ImageSharePU");
        em = emf.createEntityManager();        
        
        em.getTransaction().begin();
        User newUser = new User(uname, pword);
        em.persist(newUser);
        em.getTransaction().commit();
        
        uuserList = em.createNamedQuery("User.findAll").getResultList();
        uuseriArray = new ArrayList<>();
        uuseriArray.addAll(uuserList);
        
                
        em.close();
        emf.close();
        
        //return uname;
    }

    /**
     * PUT method for updating or creating an instance of LoginResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    
    
    
    @PUT
    @Consumes("application/xml")
    public void putXml(String content) {
    }
    
    public JsonObject jsonUserBuilder(User uuseri,boolean huuben){
        JsonObjectBuilder jBuilder = Json.createObjectBuilder();
        JsonObject jo;
        
        jBuilder = jBuilder.add("username",uuseri.getUname())
                           .add("pword", uuseri.getPword())
                           .add("uid", uuseri.getUid())
                           .add("userExists", huuben);
        jo = jBuilder.build();
        return jo;
    }
}
