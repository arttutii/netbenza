/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webjavafiles;

import DBFiles.Comment;
import DBFiles.Image;
import DBFiles.Tag;
import DBFiles.User;
import com.sun.xml.rpc.encoding.soap.CollectionSerializer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.json.Json;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;

/**
 * REST Web Service
 *
 * @author Daniel
 */
@Path("gallery")
public class GalleryResource {

    //String searchTags = "huuben pena";

    EntityManagerFactory emf;
    EntityManager em;
    ArrayList<User> uuseriArray;
    List uuserList;
    String jee;

    @Context
    private UriInfo context;

    public GalleryResource() {
    }

    //removing an image

    @POST
    @Path("deleteImg/{imgidd}")
    public void delImg(@PathParam("imgidd") int imgid) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImageSharePU");
        EntityManager em = emf.createEntityManager();
        Image delImge = null;

        em.getTransaction().begin();
        delImge = (Image) em.createNamedQuery("Image.findByImgid").setParameter("imgid", imgid).getSingleResult();
        em.remove(delImge);
        em.getTransaction().commit();
    }

    //removing an user
    @POST
    @Path("deleteUsr/{usrID}")
    public void delUsr(@PathParam("usrID") int usrID) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImageSharePU");
        EntityManager em = emf.createEntityManager();
        User delUsr = null;

        em.getTransaction().begin();
        delUsr = (User) em.createNamedQuery("User.findByUid").setParameter("uid", usrID).getSingleResult();
        em.remove(delUsr);
        em.getTransaction().commit();
    }

    //GET ALL IMAGES EVER return as json array
    @GET
    @Path("gallery")
    @Produces("application/json")
    public JsonArray fullGallery() {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImageSharePU");
        EntityManager em = emf.createEntityManager();

        //  List perslist = em.createNamedQuery("Image.findAll").getResultList();
        ArrayList<Image> imgArr = new ArrayList<>();
        imgArr.addAll(em.createNamedQuery("Image.findAll").getResultList());
        Collections.reverse(imgArr);
        JsonArray jsonImgArray = buildJsonFromArray(imgArr);

        emf.close();

        return jsonImgArray;
    }

    //GETTING COMMENTS FOR IMAGE
    //pass id of image through URL
    @GET
    @Path("comment/{param1}")
    @Produces("application/json")
    public JsonArray getComment(@PathParam("param1") int param1) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImageSharePU");
        EntityManager em = emf.createEntityManager();
        ArrayList<Comment> allComments = new ArrayList<Comment>();
        List huuben = em.createNamedQuery("Comment.findAll").getResultList();
        allComments.addAll(huuben);

        JsonObjectBuilder jBuilder = Json.createObjectBuilder();
        JsonObject jo = jBuilder.build();
        JsonArrayBuilder jaBuilder = Json.createArrayBuilder();

        for (Comment c : allComments) {
            if (c.getCimgID().getImgid() == param1) {
                jBuilder = jBuilder.add("comment_text", c.getComment())
                        .add("commenter", c.getCuserID().getUname());
                jo = jBuilder.build();
                jaBuilder.add(jo);
            }
        }
        JsonArray jsonCommentArray = jaBuilder.build();
        emf.close();
        return jsonCommentArray;
    }

    //POSTING COMMENTS 
    //pass submitter name and image id through URL
    @POST
    @Path("post_comment/{img_id}/{usr}/{comment_text}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("text/plain")
    public String postComment(@PathParam("comment_text") String comment_string,
            @PathParam("img_id") int img_id,
            @PathParam("usr") String usr
            ) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImageSharePU");
        EntityManager em = emf.createEntityManager();

        ArrayList<Comment> allComments = new ArrayList<Comment>();
        List huuben = em.createNamedQuery("Comment.findAll").getResultList();
        allComments.addAll(huuben);
        em.getTransaction().begin();
        User tempuser = (User) em.createNamedQuery("User.findByUname").setParameter("uname", usr).getSingleResult();
        Image tempimg = (Image) em.createNamedQuery("Image.findByImgid").setParameter("imgid", img_id).getSingleResult();

        Comment newComment = new Comment(comment_string, tempuser, tempimg);
        em.persist(newComment);
        em.getTransaction().commit();
        emf.close();
        return newComment.toString();/*
        
         */    }

    //SEARCH WITH USERNAME
    @POST
    @Path("searchUser")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    public String searchUsrImg(@FormParam("searchUserField") String uname) {
        /*
         EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImageSharePU");
         EntityManager em = emf.createEntityManager();
         ArrayList<Image> resutlImages = new ArrayList<>();

         User tempuser = null;
         ArrayList<Image> allimages = new ArrayList<>();
         JsonArray jsonImgArray = Json.createArrayBuilder().build();
         try {
         tempuser = (User) em.createNamedQuery("User.findByUname").setParameter("uname", uname).getSingleResult();

         if (tempuser != null) {
         allimages.addAll(em.createNamedQuery("Image.findAll").getResultList());

         for (Image i : allimages) {
         if (i.getUImgid() != null) {
         if (i.getUImgid().getUid() == tempuser.getUid()) {
         resutlImages.add(i);
         }
         }
         }
         jsonImgArray = buildJsonFromArray(resutlImages);

         }

         } catch (Exception e) {
         e.printStackTrace();

         } finally {
         emf.close();
         }
         return jsonImgArray.toString();*/
        return "ei saatana";
    }

    @GET
    @Path("getSingleImg/{value1}")
    @Produces("application/json")
    public JsonArray searchByUID(@PathParam("value1") int value1) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImageSharePU");
        EntityManager em = emf.createEntityManager();
        ArrayList<Image> allimages = new ArrayList<>();
        ArrayList<Image> finalImages = new ArrayList<>();
        em.getTransaction().begin();
        allimages.addAll(em.createNamedQuery("Image.findAll").getResultList());
        

        for (Image i : allimages) {
            
            if (i.getImgid() == value1) {
                finalImages.add(i);
            }           

        }
        return buildJsonFromArray(finalImages);

    }

    @GET
    @Path("searchTags/{value1}")
    @Produces("application/json")
    public JsonArray searchByTags(@PathParam("value1") String value1) {
        //String orderId = input.
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImageSharePU");
        EntityManager em = emf.createEntityManager();
        ArrayList<Image> allimages = new ArrayList<>();
        ArrayList<Image> finalImages = new ArrayList<>();
        em.getTransaction().begin();
        allimages.addAll(em.createNamedQuery("Image.findAll").getResultList());
        String[] splited = value1.split("\\s+");

        for (Image i : allimages) {
            for (String s : splited) {
                if (i.getUImgid() != null && i.getTitle() != null) {
                    if (i.getUImgid().getUname().equals(s) || i.getTitle().equals(s)) {
                        finalImages.add(i);
                    }
                }
                ArrayList<Tag> purettu = new ArrayList<>(i.getTagCollection());
                for (Tag t : purettu) {
                    if (t.getTname().equals(s)) {
                        finalImages.add(i);
                    }
                }
            }

        }
        return buildJsonFromArray(finalImages);
    }

    @POST
    @Path("upvote/{imgid}")
    public void upVoteImage(@PathParam("imgid") int imgid) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImageSharePU");
        EntityManager em = emf.createEntityManager();        
            try {
                emf = Persistence.createEntityManagerFactory("ImageSharePU");
                em = emf.createEntityManager();
                em.getTransaction().begin();
                Image upvteTestImg = (Image) em.createNamedQuery("Image.findByImgid").setParameter("imgid", imgid).getResultList().get(0);
                upvteTestImg.setVotes(upvteTestImg.getVotes() + 1);
                em.getTransaction().commit();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                emf.close();            
        }
    }
    @POST
    @Path("downvote/{imgid}")
    public void downVoteImage(@PathParam("imgid") int imgid) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImageSharePU");
        EntityManager em = emf.createEntityManager();        
            try {
                emf = Persistence.createEntityManagerFactory("ImageSharePU");
                em = emf.createEntityManager();
                em.getTransaction().begin();
                Image upvteTestImg = (Image) em.createNamedQuery("Image.findByImgid").setParameter("imgid", imgid).getResultList().get(0);
                upvteTestImg.setVotes(upvteTestImg.getVotes() - 1);
                em.getTransaction().commit();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                emf.close();            
        }
    }
    /*
    @GET
    @Path("getVotes/{imgid]")
    public int getVotes(@PathParam("imgid")int imgid){
        Image upvteTestImg = (Image) em.createNamedQuery("Image.findByImgid").setParameter("imgid", imgid).getResultList().get(0);
        try {
                emf = Persistence.createEntityManagerFactory("ImageSharePU");
                em = emf.createEntityManager();
                em.getTransaction().begin();
                upvteTestImg = (Image) em.createNamedQuery("Image.findByImgid").setParameter("imgid", imgid).getResultList().get(0);
                em.getTransaction().commit();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                emf.close();      
        }return upvteTestImg.getVotes();
    }*/
    

    public JsonArray buildJsonFromArray(ArrayList<Image> imgArr) {

        JsonObjectBuilder jBuilder = Json.createObjectBuilder();
        JsonObject jo = jBuilder.build();
        JsonArrayBuilder jaBuilder = Json.createArrayBuilder();

        for (Image i : imgArr) {

            jBuilder = jBuilder.add("path", "http://192.168.56.1/uploads/" + i.getImgid())
                    .add("uid", i.getImgid())
                    .add("title", i.getTitle())
                    .add("votes", i.getVotes());

            //check if uploaded by a user , if not default to Guest
            if (i.getUImgid() != null) {
                jBuilder.add("uploader", i.getUImgid().getUname());
            } else {
                jBuilder.add("uploader", "Guest");
            }
            //check the same for subdate as above
            if (i.getSubdate() != null) {
                jBuilder.add("subdate", i.getSubdate().toString());
            } else {
                jBuilder.add("subdate", "--.--.----");
            }
            jo = jBuilder.build();
            jaBuilder.add(jo);
        }
        JsonArray jsonImgArrayMethod = jaBuilder.build();
        return jsonImgArrayMethod;
    }

    public Object getSingleEntity(int i) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImageSharePU");
        EntityManager em = emf.createEntityManager();

        ArrayList<Object> allObjs = new ArrayList<Object>();
        List huuben = null;
        switch (i) {
            case 1:
                huuben = em.createNamedQuery("Comment.findAll").getResultList();
                break;
            case 2:
                huuben = em.createNamedQuery("User.findAll").getResultList();
                break;
            case 3:
                huuben = em.createNamedQuery("Image.findAll").getResultList();
                break;
        }

        allObjs.addAll(huuben);
        return allObjs.get(0);
    }

    public java.sql.Date createDBDate() {
        java.util.Date jDate = new java.util.Date();

        java.sql.Date sqlDate = new java.sql.Date(jDate.getTime());
        return sqlDate;
    }

}
