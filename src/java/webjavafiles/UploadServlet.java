/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webjavafiles;

import DBFiles.Image;
import DBFiles.Tag;
import DBFiles.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Daniel
 */
@WebServlet(name = "UploadServlet", urlPatterns = {"/up"})
@MultipartConfig(location = "/var/www/html/uploads")
public class UploadServlet extends HttpServlet {

    public void commitTags(List<Tag> lol) {
        emf = Persistence.createEntityManagerFactory("ImageSharePU");
        em = emf.createEntityManager();

        List allTags = em.createNamedQuery("Tag.findAll").getResultList();
        ArrayList<Tag> allTagsArr = new ArrayList<>();
        allTagsArr.addAll(allTags);
        boolean atLeatOne = false;
        for (Tag t : lol) {
            for (Tag tt : allTagsArr) {
                if (tt.getTname().equals(t.getTname())) {

                } else {
                    if (!em.getTransaction().isActive()) {
                        em.getTransaction().begin();
                    }
                    em.persist(t);
                    atLeatOne = true;
                    //em.getTransaction().commit();
                }
            }
        }
        if (atLeatOne) {
            em.getTransaction().commit();
        }
    }

    EntityManagerFactory emf;
    EntityManager em;
    List perslist;
    ArrayList<Image> imgArr;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            //out.println("<!DOCTYPE html>");
            //out.println("<html>");
            //out.println("<head>");
            //out.println("<title>Servlet UploadServlet</title>");
            //out.println("</head>");
            //out.println("<body>");
            //out.println("<h1>Servlet UploadServlet at " + request.getContextPath() + "</h1>");
            //out.println("</body>");
            //out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(/*@FormParam("imgtitle") String imgTitle, */HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        emf = Persistence.createEntityManagerFactory("ImageSharePU");
        em = emf.createEntityManager();
        List onlyuser = em.createNamedQuery("User.findByUname").setParameter("uname", request.getParameter("submitter")).getResultList();
        User tempuser;
        Image upImg;
        if (onlyuser.size() != 0) {
            tempuser = (User) onlyuser.get(0);
            upImg = new Image(request.getParameter("imgtitle"), tempuser, createDBDate());
        } else {
            upImg = new Image(request.getParameter("imgtitle"),createDBDate());
        }

        try {
            
            ////out.println("WTF of the first kind" + upImg.toString());

            if (!request.getParameter("tags").isEmpty()) {

                String tagsFull = request.getParameter("tags");
                String[] splited = tagsFull.split("\\s+");
                List<Tag> tagsObjs = new ArrayList<Tag>();

                //out.println("WTF BEFORE " + splited.length + "<br>");
                for (String s : splited) {
                    //out.println(s + "<br>");
                    //out.println("|| " + upImg.toString() + "|| this is upImg");
                    tagsObjs.add(new Tag(s));

                }
                commitTags(tagsObjs);
                if (!em.getTransaction().isActive()) {
                        em.getTransaction().begin();
                    }
                List<Tag> fullTags = new ArrayList<>();
                //out.println("WTF " + splited.length + "transaction status: " + em.getTransaction().isActive() + "<br>");
                for (int i = 0; i < splited.length - 1; i++) {
                    //out.println("WTF before if transaction" + em.getTransaction().isActive() + "<br>");
                    
                    
                    List<Tag> tempList = em.createNamedQuery("Tag.findByTname").setParameter("tname", splited[i]).getResultList();
                    fullTags.addAll(tempList);

                    //out.println("WTF before commit<br>");
                }

                for (Tag t : tagsObjs) {
                    Collection<Image> tempImgC = t.getImageCollection();
                    tempImgC.add(upImg);
                    t.setImageCollection(tempImgC);

                }
                upImg.setTagCollection(fullTags);
            }

            em.persist(upImg);

            perslist = em.createNamedQuery("Image.findAll").getResultList();
            imgArr = new ArrayList<>();
            imgArr.addAll(perslist);
            //out.print(perslistArr.toString());
            em.getTransaction().commit();
        } catch (Exception e) {
            out.print("erorrrr  ----  " + e.toString() + "linenum   --- " + Thread.currentThread().getStackTrace()[2].getLineNumber() + "  >---");
            Throwable cause = null; 
            Throwable result = e;
            while(null != (cause = result.getCause())  && (result != cause) ) {
                result = cause;
            }
            out.printf(" root cauasesesese    ---     " + result.getStackTrace()[2].getLineNumber());
        }

        try {

            String imageTitle = imgArr.get(imgArr.size() - 1).getImgid().toString();
            
            request.getPart("uppedfile").write(imageTitle);

            out.print("<h1>yks testi</h1>");
            out.print(imgArr.get(imgArr.size() - 1).toString() + "<img src=\"http://192.168.56.1/uploads/" + imgArr.get(imgArr.size() - 1).getImgid() + "\"/>");
        } catch (Exception e) {
            //out.println("Exception -->" + e.getMessage());
        } finally {
            out.close();

            emf.close();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public java.sql.Date createDBDate(){
        java.util.Date jDate = new java.util.Date();

        java.sql.Date sqlDate = new java.sql.Date(jDate.getTime());
        return sqlDate;
    }
    
}
