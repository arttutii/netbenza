$(document).ready(function () {
    $("#imgvotes").text();
    $('.c-hamburger').hide();
    $("#logout").hide();
    $('.registeration').hide();
    $("#mainImage").attr("src", "http://192.168.56.1/uploads/" + sessionStorage.getItem('imageID'));
    //alert("http://192.168.56.1:8080/main/main/gallery/getSingleImg/" + sessionStorage.getItem("imageID"));
    $.getJSON("http://192.168.56.1:8080/main/main/gallery/getSingleImg/" + sessionStorage.getItem('imageID'), function (data) {

        $.each(data, function (index, value) {
            $("#imgvotes").text("Votes: " + value.votes);
            if (value.submitter != undefined) {
                $(".content").prepend("<p class='imgheader'>" + value.title + "</p><br><p class='sub'>" + "Submitted on: " + value.subdate + "\nby: " + value.submitter + "</p>");
            }
            else {
                $(".content").prepend("<p class='imgheader'>" + value.title + "</p><br><p class='sub'>" + "Submitted on: " + value.subdate + "\nby: Guest</p>");
            }
        });
    });
    function getVotes() {
        $.getJSON("http://192.168.56.1:8080/main/main/gallery/getSingleImg/" + sessionStorage.getItem('imageID'), function (data) {

            $.each(data, function (index, value) {
                $("#imgvotes").text("Votes: " + value.votes);
            });
        });
    }

    $.getJSON("http://192.168.56.1:8080/main/main/gallery/comment/" + sessionStorage.getItem('imageID'), function (data) {
//alert(data);
        $.each(data, function (index1, value1) {
//alert(value1.commenter);
            $(".kommentit").append("<li><p class='kommentti'>" + value1.commenter + ": " + value1.comment_text + "</p></li><br>");
        });
    });
    $("#upvote").click(function () {
        $.ajax({
            type: "POST",
            url: "http://192.168.56.1:8080/main/main/gallery/upvote/" + sessionStorage.getItem("imageID")

        });
        getVotes();
        window.location.reload(true);
    });
    $("#downvote").click(function () {
        $.ajax({
            type: "POST",
            url: "http://192.168.56.1:8080/main/main/gallery/downvote/" + sessionStorage.getItem("imageID")

        });
        getVotes();
        window.location.reload(true);
    });
    if (sessionStorage.getItem("sessionName") != null) {
        $('.accountinfo').text("You are logged in as: " + sessionStorage.getItem("sessionName"));
    } else {
        $('.accountinfo').text("You are not logged in.");
    }

    if (sessionStorage.getItem("sessionUid") != null) {
        $("#signin").hide();
        $("#logout").show();
    }
    else {
        $("#logout").hide();
        $("#yourimages").hide();
    }
//$('#search_button').hide();

    /*if (sessionStorage.getItem("sessionName") != null) {
     $('#logout').show();
     $('#signin').hide();
     }*/

    $('#logout').click(function () {
        sessionStorage.removeItem("sessionName");
        sessionStorage.removeItem("sessionUid");
        window.location.reload(true);
    });
        //upload button
        $("#uploadbtn").click(function () {
//alert("uploading");
            $("#upload_form").ajaxSubmit();
            return false;
        });
        $("#comment_send_btn").click(function () {
            if (sessionStorage.getItem("sessionName") != null) {
                $.ajax({
                    type: "POST",
                    url: "http://192.168.56.1:8080/main/main/gallery/post_comment/" + sessionStorage.getItem("imageID") + "/" + sessionStorage.getItem("sessionName") + "/" + $("#comment_text_field").val()

                });
                location.reload(true);
            }
            else if ($("#comment_text_field").val() == "") {
                alert("Please type a message first.")
            }

            else {
//alert("http://192.168.56.1:8080/main/main/gallery/post_comment/"+sessionStorage.getItem("imageID") + "/Guest/" + $("#comment_text_field").val());
                $.ajax({
                    type: "POST",
                    url: "http://192.168.56.1:8080/main/main/gallery/post_comment/" + sessionStorage.getItem("imageID") + "/Guest/" + $("#comment_text_field").val()

                });
                location.reload(true);
            }
        });
            $("#regis").click(function () {
                $('.signature').hide();
                $('.registeration').fadeIn();
            });
            $("#logins").click(function () {
                $('.registeration').hide();
                $('.signature').fadeIn();
            });
            $("#myBtn").click(function () {
                $("#myModal").modal({backdrop: true});
            });
            $("#signin").click(function () {
                $("#loginModal").modal({backdrop: true});
            });
            /* hamburger businesssss !!! */
//JAVASCRIPT:

// Function for the hamburger menu animation
            (function () {

                "use strict";
                var toggles = document.querySelectorAll(".c-hamburger");
                for (var i = toggles.length - 1; i >= 0; i--) {
                    var toggle = toggles[i];
                    toggleHandler(toggle);
                }
                ;
                function toggleHandler(toggle) {
                    toggle.addEventListener("click", function (e) {
                        e.preventDefault();
                        (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
                    });
                }

            })();
            /*MODAL TRIGGERS!!*/




            $("#search_button").click(function () {
                $(".content").empty();
                $.getJSON("http://192.168.56.1:8080/main/main/gallery/searchTags/" + $("#tagsearch").val(), function (data) {
                    $.each(data, function (index1, value1) {
                        $('.content').append("<div class='images'><img alt='" + value1.title + "' id='" + value1.uid + "'class='sensabensa img-responsive center-block' src='" + value1.path + "'>\n\
                                                    <h1>\"" + value1.title + "\"</h1></div>");
                    });
                    $("img").click(function () {
                        //alert("ei toimi");
                        sessionStorage.setItem("imageID", $(this).attr("id"));
                        window.location = "imageIndex.html";
                    });
                });
            });
            
            $("#yourimages").click(function () {
                $(".content").empty();
                $.getJSON("http://192.168.56.1:8080/main/main/gallery/searchTags/" + sessionStorage.getItem("sessionName"), function (data) {
                    $.each(data, function (index1, value1) {
                        $('.content').append("<div class='images'><img alt='" + value1.title + "' id='" + value1.uid + "'class='sensabensa img-responsive center-block' src='" + value1.path + "'>\n\
                                    <h1>\"" + value1.title + "\"</h1></div>");
                    });
                    $("img").click(function () {
                        //alert("ei toimi");
                        sessionStorage.setItem("imageID", $(this).attr("id"));
                        window.location = "imageIndex.html";
                    });
                });
            });
            
            
            $("#tagsearch").keydown(function (event) {
                if (event.keyCode == 13) {
                    $('#search_button').click();
                    $(".content").empty();
                    return false;
                }
            });

            $('#logen').click(function () {

        var gei = "http://192.168.56.1:8080/main/main/login/logIn/" + $('#username').val() + "/" + $('#password').val();
        alert(" logen" + gei);
        $.getJSON(gei, function (data) {
            if (data.userExists) {
                $.each(data, function (index1, value1) {
                    sessionStorage.setItem("sessionName", data.username);
                    sessionStorage.setItem("sessionUid", data.uid);
                });
                window.location.reload(true);
            }
            else {
                alert("Invalid credentials. Please check your input or register an account");
            }
        });
        $("#loginModal").modal("toggle");
    });
            
           $('#register').click(function () {
        if($("#regisuname").val() != null && $('#regispword').val() != null && $('#regispword2').val()==$('#regispword').val()){
            $.ajax({
                type: "POST",
                url: "http://192.168.56.1:8080/main/main/login/newUser/" + $('#regisuname').val() + "/" + $('#regispword').val()
            });
            alert("New user created! Now please log in with your new account!");
        }
        else{
            alert("Invalid username or password. Plese check your input. ");
        }
            $('.registeration').hide();
            $('.signature').fadeIn();
    });
            
            //resize
           
            $(".content").append("<img src='http://192.168.56.1/uploads/" + sessionStorage.getItem("imageID") + ">");
        });